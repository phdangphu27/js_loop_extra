const numberEl = document.getElementById("number");
const resultEl = document.getElementById("result");

const check = () => {
  const numberVal = numberEl.value;
  let resultHTML = "";

  for (let i = 1; i <= numberVal; i++) {
    let count = 0;
    for (let j = 1; j <= i; j++) {
      if (i % j === 0) {
        count++;
      }
    }
    if (count === 2) {
      resultHTML += ` ${i} `;
    }
  }

  resultEl.innerHTML = resultHTML;
};
